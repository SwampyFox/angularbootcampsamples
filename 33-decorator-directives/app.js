(function (angular) {
  "use strict";

  angular.module('exampleApp', ['drag'])
    .directive('bounce', function ($interval) {
      return {
        restrict: 'A',
        link: function (scope, element, attrs) {
          var n = 0;
          var intervalId = $interval(function () {
            n += 0.1;
            element.css({
              "transform": "rotate(" + Math.sin(n) * 5 + "deg)" //
            });
          }, 25);

          element.on('$destroy', function () {
            $interval.cancel(intervalId);
          });
        }
      };
    })
    .directive('blink', function ($interval) {
      return {
        restrict: 'A',
        link: function (scope, element, attrs) {
          var on = true;
          var intervalId = $interval(function () {
            on = !on;
            element.css({
              display: on ? 'inline-block' : 'none'
            });
          }, 500);

          element.on('$destroy', function () {
            $interval.cancel(intervalId);
          });
        }
      };
    });

})(window.angular);
