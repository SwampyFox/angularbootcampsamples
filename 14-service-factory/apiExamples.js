(function(angular) {
  'use strict';

  angular.module('apiExamples', [])

  .controller('AController', function (myService, myFactory, justSayHello, BASEAPIURL, justSayHelloService) {
    this.greeting1 = myService.sayHello("Joe");
    this.greeting2 = myFactory.sayHello("Joe");
    this.greeting3 = justSayHelloService.justSayHello("Joe");
    this.n = BASEAPIURL;
  })

  /* Javascript constructor functions look like this:
  function Foo() {
    this.prop1 = "eee";
  }

  var s = new Foo();

  var f = { prop1: "eee" };
  */

  .service('myService', function ( /* DI */ ) {
    // service is just a constructor function
    // that will be called with 'new'
    var mys = this;
    this.sayHello = function (name) {
      return "Hi " + name + "!";
    };
    this.fullGreeting = function (n) {
      return "Greetings;" + mys.sayHello(n);
    };
  })

  .factory('myFactory', function ( /* DI */ ) {
    // factory returns an object
    // you can run some code before
    function sayHello(name) {
      return "Hi " + name + "!";
    }
    function fullGreeting(n) {
      return "Greetings;" + sayHello(n);
    }
    return {
      sayHello: sayHello,
      fullGreeting: fullGreeting
    };
  })

  .factory('justSayHello', function ( /* DI */ ) {
    return function justSayHello(name) {
      return "Hi " + name + "!";
    };
  })
  .service('justSayHelloService', function () {
      this.justSayHello = function (name) {
          return "Hi " + name + "!";
      }
  });

})(window.angular);
