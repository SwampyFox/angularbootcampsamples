(function (angular) {
  "use strict";

  angular.module('exampleApp', ['apiExamples', 'countries'])
    .constant('BASEAPIURL', "http://mysite.com")
    .value('closerToPi', 3.14159);

})(window.angular);
