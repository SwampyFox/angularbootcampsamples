(function (angular) {
  "use strict";

  angular.module('simpleApplication', ['ngRoute'])
    .config(
      function ($routeProvider) {
        $routeProvider.
          when('/welcome', {
            template: '<welcome></welcome>'
          }).
          otherwise({
            redirectTo: '/welcome'
          });
      })
    .controller('WelcomeController', WelcomeController)
    .component('welcome', {
        templateUrl: 'welcome.html',
        controller: 'WelcomeController',
        controllerAs: 'wc'
    });

  function WelcomeController() {
    this.who = 'World';
  }

})(window.angular);
