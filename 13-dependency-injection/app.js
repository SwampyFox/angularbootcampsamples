(function (angular) {
  "use strict";

  // Consider the parameter list in the following line
  function InjectionController($timeout, $log, $interval, $location) {
    var ic = this;

    function writeToLog() {
      $log.info('Hello World');
    }

    ic.counter = 1;
    ic.url = $location.absUrl();

    function incCounter() {
      ic.counter++;
    }

    $timeout(writeToLog, 1000);
    $interval(incCounter, 400);
  }

  angular.module('app', [])
    .controller('InjectionController', InjectionController);

})(window.angular);
