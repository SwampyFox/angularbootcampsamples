(function (angular) {
  "use strict";

  angular.module("myApp", [])
    .component("cardBlock", {
        templateUrl: "card.html"
    });
})(window.angular);
