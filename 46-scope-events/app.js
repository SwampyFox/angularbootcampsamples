(function (angular) {
  "use strict";

  function ListenerController($scope) {
    var lc = this;
    $scope.$on("nameChange", function (event, newName) {
      // This can cause trouble in some IEs
      console.log(event);
      lc.childName = newName;
    });
  }

  function EmitController($scope) {
    var ec = this;
    ec.name = "John";
    ec.changeName = function () {
      $scope.$emit("nameChange", ec.name);
    };
  }

  angular.module("exampleApp", [])
    .controller("ListenerController", ListenerController)
    .controller("EmitController", EmitController);

})(window.angular);
