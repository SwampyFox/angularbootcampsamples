(function (angular) {
  "use strict";

  function CountryController(loginService, dataService) {
    var cc = this;
    cc.loginStatus = false;
    var loginResultPromise;

    cc.login = function () {
      // get a promise
      loginResultPromise = loginService.login();

      // Update view with status from promise
      loginResultPromise.then(function (result) {
        cc.loginStatus = result;
      });

      // Go get some other data
      loginResultPromise.then(function (result) {
        console.log('id: ', result.id);
        if (result) {
          return loginService.getSomeData().then(function (data) {
              cc.loginData = data;
          });
        }
      });

      loginResultPromise.then(function (data) {
          console.log(data);
      });

      // pass it to a function (which calls .then on it)
      dataService.processLogin(loginResultPromise);
    };

  }

  function LoginService($http) {
    var ls = this;
    var userPromise = $http.get('../demo-data/workers.json').then(function (response) {
      ls.currentUser = response.data[0];
      return ls.currentUser;
    });
    this.login = function () {
      return userPromise.then(function (currentUser) {
        console.log('data is: ', currentUser);
        //very naive login service
        if (currentUser.id === 1) {
          return currentUser;
        }
        return false;
      });
    };

    var dataPromise = $http.get('/demo-data/colors.json').then(res => res.data);
    this.getSomeData = function () {
      return dataPromise;
    };
  }

  function DataService () {
      this.processLogin = function (loginPromise) {
          loginPromise.then(function () {
              console.log('login processed');
          })
      }
  }

  angular.module('app', [])
    .controller('CountryController', CountryController)
    .service('loginService', LoginService)
    .service('dataService', DataService);

})(window.angular);
