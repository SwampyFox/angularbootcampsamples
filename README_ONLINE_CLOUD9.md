# Angular Boot Camp - Learning Materials

http://angularbootcamp.com/

## Welcome

Welcome to the Oasis Digital Angular Bootcamp

While we wait for others to arrive you can get
a jumpstart by performing the following steps:

1. wget http://angularbootcamp.com/learn.zip
2. Extract the zip: unzip learn.zip
3. Download live server: sudo npm install -g live-server
4. Navigate to the Learn directory: cd Learn
5. Start the server: live-server
6. Open your application in another browser tab: 
http://yourWorkspaceName-yourUsername.c9users.io/

Open this file (README_ONLINE.md) for more details.

-----------------------------------------------------------------------------------

## "Learn" - the first part of the class

This is a standalone, ready to use set of files, sufficient for the first portion of the class.
The only tools truly necessary for this portion are a text editor and a Web server.
See the readme file inside for instructions on how to launch a Web server,
or use any Web server you have handy.

## Development Web Server Needed

You need to serve these files via a Web server. Your Cloud9 environment has
Node pre-installed, which includes NPM, its package manager. Use that to install 
live-server, a very convenient development Web server.

```
npm install -g live-server
```

Navigate to the Learn directory (cd Learn), then run the server:

```
live-server
```

Live server conveniently includes "live reload" in the box.
It will automatically inject pages it serves with a bit of JavaScript to access a web socket
to automatically reload the page from the files change.
This means you will not have to click refresh in the browser.

**Important**: Serve the "learn" directory as your "web root".
You will not need to restart your Web server as we move from one step to the next,
and each example will be able to see the shared library files and data files.

This is a very minimal use of Node/NPM; in later work,
we will see more of how to use it effectively and idiomatically in an Angular project.

## Optional: Karma

Some steps provide a few opportunities to introduce unit testing with Karma.
Karma can be installed (if desired) like so:

```
npm install -g karma karma-jasmine karma-chrome-launcher
```

