(function(angular) {
  'use strict';

  angular.module('app', [])
    .controller('OuterController', OuterController)
    .component('capitalDisplay', {
      template: '<h2>The capital of {{$ctrl.countryName}} is {{$ctrl.capName}}</h2>',
      bindings: {
        capName: '@capital',
        countryName: '@countryName'
      }
    })
    .component('myCountry', {
      templateUrl: 'country-template.html',
      bindings: {
        country: '=country'
      },
      controllerAs: 'cc'
    });

  /*
    .directive('myCountry', function(){
      return {
        templateUrl: 'country-template.html',
        scope: true,
        bindToController: true,
        scope: {
          country: '=country'
        },
        controller: myCountryController,
        controllerAs: 'cc'
      }
    })

    //need empty controller to support directive
    function myCountryController(){

    }
  See Legacy Steps for more examples
  */

  function OuterController() {
    this.canada = {
      'countryCode': 'CA',
      'countryName': 'Canada',
      'population': 33679000,
      'capital': 'Ottawa',
      'areaInSqKm': 9984670
    };

    this.usa = {
      'countryCode': 'US',
      'countryName': 'United States',
      'population': 310232863,
      'capital': 'Washington',
      'areaInSqKm': 9629091
    };
  }
})(window.angular);
