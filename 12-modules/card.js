(function (angular) {
  "use strict";

  angular.module("card", [])
    .component("cardBlock", {
        templateUrl: "card.html",
    })

    .controller("PersonController", function PersonController() {
      this.name = "The Wrong Name";
      this.deck = [1, 2];
    });

})(window.angular);
