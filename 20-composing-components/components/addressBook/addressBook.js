(function (angular) {
  "use strict";

  angular.module("addressBook", ["addressEntry", "expando"])
    .service('addressService', AddressService)
    .component("addressBook", {
        templateUrl: "components/addressBook/addressBook.html",
        controller: AddressBookController,
        controllerAs: "bc",
        bindings: {
          purpose: "@purpose"
        }
    });

    function AddressBookController(addressService) {
      var abc = this;

      addressService.addressPromise.then(function (data) {
        abc.addresses = data;
      });
      abc.add = function add() {
        abc.addresses.push({
          heading: "New"
        });
      };

      // This does not work, because binding happens after the controller.
      abc.purpose = abc.purpose + " and more";
    };

    function AddressService($http) {
      var as = this;

      as.addressPromise = $http.get("data.json").then(function (response) {
        return response.data;
      });
    }

})(window.angular);
