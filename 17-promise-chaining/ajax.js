(function ajax(angular) {
  'use strict';

  function CountryController(countryService) {
    var cc = this;
    countryService.getList().then(function(list) {
      cc.countries = list;
    });

    countryService.getListLength().then(function(len) {
      cc.listLength = len;
    });
  }

  function CountryService($http) {
    var url = '../demo-data/countries.json';
    var countryService = this;
    function log(param) {
      console.log(param);
      return param;
    }

    countryService.getList = function() {
      // Eventually resolves with the response
      var httpPromise = $http.get(url);

      // Eventually resolves with the data extracted from the previous result
      var dataPromise = httpPromise.then(function(response) {
        return response.data;
      });

      return dataPromise;
    };

    countryService.getList2 = function() {
      // This is how we would write getList()
      return $http.get(url).then(function(response) {
        return response.data;
      });
    };

    countryService.getList3 = function() {
      return $http.get(url)
        .catch(function (err) {
          console.error("Got an error, using default data", err);
          return {
            data: 'default'
          }
        })
        // Note that we are back on the success path, we have caught the error
        .then(function (response) {
          return response.data;
        });
    };
    
    countryService.getList4 = function (){
      return $http.get(url)
        .then(function (response) {
          return response.data;
        }, function (err){
          console.error("Got an error, using default data", err);
          return 'default';
        });
    }

    countryService.getListLength = function() {
      var x = $http.get(url)
        .then(log)
        // The promise here wraps the return value of the callback and
        // sends it to the next then function
        .then(function(response) { return response.data; })
        .then(log)
        .then(function(list) { return list.length; })
        .then(log);
      console.log('Done setting up chain');
      return x;
    };

    // Who wants to dive into ES6?
    countryService.getListLength2 = () =>
      $http.get(url)
        .then(response => response.data)
        .then(list => list.length);
  }

  angular.module('app', [])
    .controller('CountryController', CountryController)
    .service('countryService', CountryService);
} (window.angular));
